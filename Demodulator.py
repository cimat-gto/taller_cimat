import numpy as np
import scipy as sp
import cv2

class Demodulator(object):
    x = 0
    y = 0
    w = 0
    h = 0

    def __init__(self, N = 256, M = 256, alpha = np.pi/2.0):
        self.N = N
        self.M = M
        self.w = N
        self.h = M
        self.alpha = alpha
        self.real = ...  # type: np.ndarray
        self.imag = ...  # type: np.ndarray
        self.phase_reference = ... # type: np.ndarray

    def set_list(self, list):

        list_lowpass = []
        for img in list:
            list_lowpass.append(cv2.blur(img, (5,5)))

        self.I = list_lowpass

        if len(self.I) >= 5:
            self.real = (2.0*self.I[2] - self.I[0] - self.I[4])*np.sin(self.alpha)
            self.imag = 2.0*self.I[1] - 2.0*self.I[3] - (self.I[0] - self.I[4])*np.cos(self.alpha)

    def remove(self, fx, fy, sign = -1):
        wm = 2.0 * np.pi * fy
        wn = 2.0 * np.pi * fx

        #Jr = np.zeros((self.N, self.M))
        #Ji = np.zeros((self.N, self.M))


        if len(self.I) == 5:
            n,m = np.ogrid[0:self.N, 0:self.M]
            s=(self.T.real + self.T.imag*1.j)* np.exp(1.j * sign * (wm*m + wn*n))
            #J = np.arctan2(s.real, s.imag)
            #for n in range(0, self.N):
            #    for m in range(0, self.M):
            #        Jr[n, m] = self.T[n, m].real * np.cos(-wm * m) - self.T[n, m].imag * np.sin(-wm * m)
            #        Ji[n, m] = self.T[n, m].imag * np.cos(-wm * m) + self.T[n, m].real * np.sin(-wm * m)

        #self.T.real = Jr
        #self.T.imag = Ji
        self.T.real = s.real
        self.T.imag = s.imag

    def remove_reference(self, p, sign=-1):
        signal_p = np.exp(p*1j)
        signal_r = np.exp(self.phase_reference*1j*sign)

        signal = signal_p*signal_r

        return np.arctan2(signal.imag, signal.real)

    def get_magnitude(self):
        return np.sqrt(self.real**2 + self.imag**2)

    def get_phase(self):
        return np.arctan2(-self.imag, self.real)

    def get_k(self):
        F = sp.fft(np.cos(np.arctan2(self.real, self.imag)))

        cmax = -1
        nmax = -1
        mmax = -1

        for n in range(0, self.N):
            for m in range(0, self.M):
                norma = np.sqrt(F[n, m]*F[n, m].conjugate()).real
                if norma > cmax:
                    cmax = norma
                    nmax = n
                    mmax = m

        print(nmax)
        print(mmax)
        return (mmax, nmax)

    def set_reference(self, p, tau=100):
        m = self.get_magnitude()
        m = cv2.normalize(m, None, 255.0, 0.0, cv2.NORM_MINMAX, cv2.CV_8U)
        ret, thresh = cv2.threshold(m, tau, 255, 0)

        im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        n_max = 0
        area = 0
        for n in range(len(contours)):
            area_cnt = cv2.contourArea(contours[n])
            if area < area_cnt:
                area = area_cnt
                n_max = n

        self.x, self.y, self.w, self.h = cv2.boundingRect(contours[n_max])

        self.phase_reference = p[self.y:self.y + self.h, self.x:self.x + self.w]
        thresh[self.y:self.y + self.h, self.x:self.x + self.w] = 128

        return thresh, self.phase_reference

    def get_subimage(self, p):

        return p[self.y:self.y+self.h, self.x:self.x+self.w]